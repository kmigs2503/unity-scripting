using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour { 


[SerializeField]
private Vector3 direction;  //posibles ejes de movimient
public float speed;         // velocidad de movimiento

    void Update()
    {
        // Acotaci�n de los valores de direcci�n al valor de unidad [-1,1 ]
        direction = ClampVector3(direction);
    
    // Desplazamiento del componente transform en base al tiempo
    transform.Translate(direction*(speed * Time.deltaTime));
    }

    /*
     * Funci�n auxiliar que permite acotar los valores de las componentes
     * de un Vector3 entre -1 y 1.
     */
    public static Vector3 ClampVector3(Vector3 target) {
        float clampedX = Mathf.Clamp(target.x, -1f, 1f);
        float clampedY = Mathf.Clamp(target.y, -1f, 1f);
        float clampedZ = Mathf.Clamp(target.z, -1f, 1f);

        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);
        // Update is called once per frame

        return result;

    }
}